// tools.js
// ========
module.exports = {
    decode: function (data) {
        let new_data = false;
        var output_data = new Float32Array(6);
        if(data[0] === 'A'.charCodeAt(0) && data[1] === 'B'.charCodeAt(0)) {
            // console.log('Header match')
            // console.log('SW: ', data[2]<<8 | data[3])
            // console.log('Lenght: ', data[4])
            for (let i = 0; i < data[4]; i++) {
                let _data = new Uint8Array(4);
                _data[0] = data[i*4+5];
                _data[1] = data[i*4+6];
                _data[2] = data[i*4+7];
                _data[3] = data[i*4+8];
                let f32 = new Float32Array(_data.buffer);
                // console.log(f32[0]);
                output_data[i] = f32[0];
            }
            var crc_calc = 0;
            for (let i = 0; i < data.length-2; i++) {
                crc_calc += data[i]
            }
            let crc_data = data[(data[4]-1)*4+9]

            if((crc_calc & 0xFF) === crc_data) {
                console.log("CRC OK", crc_calc & 0xFF,"===", crc_data)
                new_data = true;
            } else {
                console.log("CRC Fail", crc_calc & 0xFF,"!=", crc_data)
            }
        }
        return [output_data, new_data];
    },
    encode: function (data) {
        // whatever
        let buf = Buffer.alloc(6);
        let n = 0;
        buf.writeUInt8(65, n++);
        buf.writeUInt8(66, n++);
        buf.writeUInt8(1, n++);
        buf.writeUInt8(1, n++);
        buf.writeUInt8(0, n++);
        buf.writeUInt8(65+66+2, n++);
        return buf;
    }
};